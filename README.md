# irCurve

The term structure of interest rates, also known as yield curve, is defined as the relationship between the yield-to-maturity on a zero coupon bond and the bond's maturity. Zero yield curves play an essential role in the valuation of all financial pr